# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

A main webcomponent file is in dist/ directory. To update dist, enter webpack command.
```
$ webpack
```

~~Open the index on a browser~~
~~$ open demo/index.html~~

local browser
```
$ live-server &
$ kill %1
$ jobs
``

## local view
http://127.0.0.1:8080/public/?test
